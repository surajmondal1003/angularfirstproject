import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer=false;
  serverName:string='';
  serverCreationStaus='No server was created';
  serverCreated=false;
  serversArr=[];
  constructor() { 
    // setTimeout(()=>{
    //   this.allowNewServer=true;
    // },2000);
  }

  ngOnInit() {
  }
   onCreateServer(){
    //console.log(this.serverName);
    this.serversArr.push(this.serverName);
    this.serverCreationStaus='Server was Created!. Name is '+this.serverName;
    this.serverCreated=true;
   }

   isallowNewServer(){
     if(this.serverName.length > 0){
      this.allowNewServer=true;
     }
     return this.allowNewServer;
   }

  //  onUpdateserverName(event:any){
  //   this.serverName=(<HTMLInputElement>event.target).value;
  //  }
}
